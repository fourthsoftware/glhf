# glhf

Glhf (Good Luck; Have Fun) is a chaos engineering application written in Kotlin. Set a scope and feed it hostnames, IPs, and secrets and let it randomly take down your servers, network, web applications, firewalls, etc. Good luck, have fun!